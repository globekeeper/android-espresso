# README #

This README explains everything that needs to be done here.

### Overview ###

GlobeKeeper as such a secured login that it even checks for a real email address and password!
But of course as a startup GlobeKeeper didn't write any Test Plans / Automated testing..

### This is where you get into the picture! ###

* Fork this repository
* Read the code, understand the flow
* Write a Test Suite using [this template](https://drive.google.com/file/d/0B0j3CJSJO1DMMmpfZGladUVON3M/view).
* Test Cases should be written so they can be automated later on
* Automate all written tests using Espresso
* Commit and push all your changes including the XLS Test Suite.
* Comments on security issues or code issues are welcome, the code is written poorly by purpose

### Good luck! ###