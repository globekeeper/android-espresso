package com.globekeeper.testapp;

import android.app.Activity;
import android.os.Build;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import androidx.test.filters.LargeTest;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static androidx.test.espresso.assertion.ViewAssertions.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

/**
 * Basic tests showcasing simple view matchers and actions like {@link ViewMatchers#withId},
 * {@link ViewActions#click} and {@link ViewActions#typeText}.
 * <p>
 * Note that there is no need to tell Espresso that a view is in a different {@link Activity}.
 */
@RunWith(AndroidJUnit4.class)
@Config(sdk = {Build.VERSION_CODES.P})
@LargeTest
public class ApplicationTest {

    /**
     * Use {@link ActivityScenarioRule} to create and launch the activity under test.
     */
    @Rule public ActivityScenarioRule<LoginActivity> activityScenarioRule =
            new ActivityScenarioRule<LoginActivity>(LoginActivity.class);

    @Test
    public void firstTest() {
        // Type text and then press the button.
        ActivityScenario<LoginActivity> scenario = activityScenarioRule.getScenario();
        onView(withId(R.id.email)).perform(typeText("test email"));
        // ... TODO
    }
}